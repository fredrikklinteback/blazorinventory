﻿using System;

namespace Dtos
{
    public class InventarieDto
    {
        public string Namn { get; set; }
        public DateTime Anskaffningsdatum { get; set; }
        public string Kommentar { get; set; }
    }
}
