﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application;
using Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InventarieController : ControllerBase
    {

        private readonly ILogger<InventarieController> _logger;
        private readonly IMediator _mediator;

        public InventarieController(ILogger<InventarieController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IList<InventarieDto>> Get()
        {
            var result = await _mediator.Send(new GetInventarier.Request());

            return result.Inventarier;
        }

        [HttpPut]
        public async Task Put([FromBody] InventarieDto inventarie)
        {
            await _mediator.Send(new PutInventarie.Request { Inventarie = inventarie});
        }
    }
}
