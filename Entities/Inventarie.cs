﻿using System;

namespace Entities
{
    public class Inventarie
    {
        public int Id { get; set; }
        public string Namn { get;set; }
        public DateTime Anskaffningsdatum { get; set; }
        public string Kommentar { get; set; }
    }
}
