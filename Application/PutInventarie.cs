﻿using Dtos;
using Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application
{
    public class PutInventarie
    {
        public class Request: IRequest
        {
            public InventarieDto Inventarie { get;set; }
        }

        public class Handler : AsyncRequestHandler<Request>
        {
            private readonly InventarieContext _inventarieContext;
            public Handler(InventarieContext inventarieContext)
            {
                _inventarieContext = inventarieContext;
            }

            protected override async Task Handle(Request request, CancellationToken cancellationToken)
            {
                _inventarieContext.Database.EnsureCreated();

                _inventarieContext.Inventaries.Add(new Inventarie
                {
                    Namn = request.Inventarie.Namn,
                    Anskaffningsdatum = request.Inventarie.Anskaffningsdatum,
                    Kommentar = request.Inventarie.Kommentar
                });

                await _inventarieContext.SaveChangesAsync();
            }
        }
    }
}
